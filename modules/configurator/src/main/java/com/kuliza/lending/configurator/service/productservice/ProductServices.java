package com.kuliza.lending.configurator.service.productservice;

import java.util.List;
import java.util.UUID;

import org.kie.api.runtime.KieContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductCategoryDao;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.ProductDeployment;
import com.kuliza.lending.configurator.models.ProductDeploymentDao;
import com.kuliza.lending.configurator.models.RuleDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.UserDao;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.KieContainerBean;
import com.kuliza.lending.configurator.pojo.SubmitNewProduct;
import com.kuliza.lending.configurator.pojo.UpdateProduct;
import com.kuliza.lending.configurator.service.genericservice.GenericServicesCE;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;

@Service
public class ProductServices extends GenericServicesCE {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private UserDao userDao;

	@Autowired
	private RuleDao ruleDao;

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private ExpressionDao expressionDao;

	@Autowired
	private ProductCategoryDao productCategoryDao;

	@Autowired
	private ProductDeploymentDao productDeploymentDao;

	@Autowired
	private KieContainerBean kieContainerBean;

	public GenericAPIResponse getProductCategories() throws Exception {
		List<ProductCategory> allProductCategories = productCategoryDao.findByIsDeleted(false);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, allProductCategories);
	}

	public GenericAPIResponse getProductsList(String userId, String productCategoryId, String status) throws Exception {
		List<Product> allUserProducts = null;
		if (productCategoryId != null) {
			allUserProducts = productDao.findByUserIdAndProductCategoryIdAndIsDeletedAndStatusGreaterThanEqual(
					Long.parseLong(userId), Long.parseLong(productCategoryId), false, Integer.parseInt(status));
		} else {
			allUserProducts = productDao.findByUserIdAndIsDeletedAndStatusGreaterThanEqual(Long.parseLong(userId),
					false, Integer.parseInt(status));
		}
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.PRODUCT_FILTER, SimpleBeanPropertyFilter
				.serializeAllExcept(Constants.EXPRESSIONS, Constants.GROUPS, Constants.VARIABLES));
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, allUserProducts);
	}

	public GenericAPIResponse getAllProductsData(String userId) throws Exception {
		List<Product> allUserProducts = productDao.findByUserIdAndIsDeleted(Long.parseLong(userId), false);
		FilterProvider filters = new SimpleFilterProvider()
				.addFilter(Constants.PRODUCT_FILTER, SimpleBeanPropertyFilter.serializeAll())
				.addFilter(Constants.GROUP_FILTER, SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, allUserProducts);
	}

	public GenericAPIResponse getSingleProduct(String productId) throws Exception {
		Product product = productDao.findByIdAndIsDeleted(Long.parseLong(productId), false);
		FilterProvider filters = new SimpleFilterProvider()
				.addFilter(Constants.PRODUCT_FILTER, SimpleBeanPropertyFilter.serializeAll())
				.addFilter(Constants.GROUP_FILTER, SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, product);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse createNewProduct(SubmitNewProduct input) throws Exception {
		User user = userDao.findById(Long.parseLong(input.getUserId()));
		String productName = input.getProductName();
		ProductCategory productCategory = productCategoryDao
				.findByIdAndIsDeleted(Long.parseLong(input.getProductCategoryId()), false);
		Product newProduct = new Product(productName, user, productCategory, UUID.randomUUID().toString());
		productDao.save(newProduct);
		if (!input.getTemplateProductId().equals("")) {
			HelperFunctions.createTemplate(Long.toString(newProduct.getId()), input.getTemplateProductId(), productDao,
					expressionDao, groupDao, ruleDao, variableDao);
		}
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.PRODUCT_FILTER, SimpleBeanPropertyFilter
				.serializeAllExcept(Constants.EXPRESSIONS, Constants.VARIABLES, Constants.GROUPS));
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, newProduct);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse updateProduct(UpdateProduct input) throws Exception {
		Product product = productDao.findByIdAndIsDeleted(Long.parseLong(input.getProductId()), false);
		product.setName(input.getProductName());
		productDao.save(product);
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.PRODUCT_FILTER, SimpleBeanPropertyFilter
				.serializeAllExcept(Constants.EXPRESSIONS, Constants.VARIABLES, Constants.GROUPS));
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, product);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse cloneProduct(String userId, String productId) throws Exception {
		User user = userDao.findById(Long.parseLong(userId));
		Product oldProduct = productDao.findByIdAndIsDeleted(Long.parseLong(productId), false);
		String newProductName = UUID.randomUUID().toString();
		Product newProduct = new Product(newProductName, user, oldProduct.getProductCategory(),
				oldProduct.getIdentifier(), oldProduct.getId());
		productDao.save(newProduct);
		newProduct.setName(oldProduct.getName() + "_" + newProduct.getId());
		HelperFunctions.createTemplate(Long.toString(newProduct.getId()), Long.toString(oldProduct.getId()), productDao,
				expressionDao, groupDao, ruleDao, variableDao);
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.PRODUCT_FILTER, SimpleBeanPropertyFilter
				.serializeAllExcept(Constants.EXPRESSIONS, Constants.VARIABLES, Constants.GROUPS));
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, newProduct);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse publishProduct(String productId) throws Exception {
		Product product = productDao.findByIdAndIsDeleted(Long.parseLong(productId), false);
		product.setStatus(1);
		productDao.save(product);
		KieContainer kieContainer = HelperFunctions.generateRulesDRLFile(productId, productDao, groupDao, ruleDao);
		kieContainerBean.setSingleContainer(Constants.TEST_ENVIRONMENT, Constants.CONTAINER_ID + productId,
				kieContainer);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.PRODUCT_PUBLISHED_SUCCESS_MESSAGE);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse editProduct(String productId) throws Exception {
		Product product = productDao.findByIdAndIsDeleted(Long.parseLong(productId), false);
		product.setStatus(0);
		productDao.save(product);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.PRODUCT_EDITABLE_SUCCESS_MESSAGE);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse deployProduct(String userId, String productId) throws Exception {
		Product product = productDao.findByIdAndIsDeleted(Long.parseLong(productId), false);
		User user = userDao.findById(Long.parseLong(userId));
		ProductDeployment productDeployment = productDeploymentDao
				.findByIdentifierAndStatusAndIsDeleted(product.getIdentifier(), true, false);
		// if not first time product deployment done against unique
		// identifier
		if (productDeployment != null) {
			productDeployment.setStatus(false);
			// currently deployed product status changed to undeployed
			Product productToUndeploy = productDeployment.getProduct();
			productToUndeploy.setStatus(3);
			productDao.save(productToUndeploy);
			productDeploymentDao.save(productDeployment);
		}
		// create new deployment
		productDeployment = new ProductDeployment(user, product, product.getIdentifier());
		productDeploymentDao.save(productDeployment);
		product.setStatus(2);
		productDao.save(product);
		KieContainer kieContainer = kieContainerBean.getSingleContainer(Constants.TEST_ENVIRONMENT,
				Constants.CONTAINER_ID + productId);
		kieContainerBean.setSingleContainer(Constants.PRODUCTION_ENVIRONMENT, Constants.CONTAINER_ID + productId,
				kieContainer);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.PRODUCT_DEPLOYED_SUCCESS_MESSAGE);
	}

}
