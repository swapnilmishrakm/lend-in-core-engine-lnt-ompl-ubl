package com.kuliza.lending.configurator.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "ce_product_deployment")
@Where(clause = "is_deleted=0")
public class ProductDeployment extends BaseModel {

	@Column(nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean status = false;
	@Column(nullable = false)
	private String identifier;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId", nullable = false)
	private Product product;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", nullable = false)
	private User user;

	public ProductDeployment() {
		super();
		this.setIsDeleted(false);
	}

	public ProductDeployment(User user, Product product, String identifier) {
		this.user = user;
		this.product = product;
		this.identifier = identifier;
		this.status = true;
		this.setIsDeleted(false);
	}

	public ProductDeployment(long id, boolean status, String identifier, Product product, User user) {
		super();
		this.setId(id);
		this.status = status;
		this.identifier = identifier;
		this.product = product;
		this.user = user;
		this.setIsDeleted(false);
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
