package com.kuliza.lending.configurator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.kie.api.runtime.KieContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.Resource;

import com.kuliza.lending.configurator.pojo.KieContainerBean;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;

import liquibase.integration.spring.SpringLiquibase;

@SpringBootApplication
@ComponentScan(basePackages = { "com.kuliza" })
@EnableConfigurationProperties(LiquibaseProperties.class)
public class CreditEngineApplication extends SpringBootServletInitializer {

	@Autowired
	private LiquibaseProperties properties;
	@Autowired
	private DataSource dataSource;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CreditEngineApplication.class);
	}

	@PostConstruct
	public void init() {
		// TimeZone.setDefault(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
		System.out.println("Current system start time :" + new Date());

	}

	public static void main(String[] args) throws IOException {
		SpringApplication.run(CreditEngineApplication.class, args);
	}

	@Bean
	public KieContainerBean getKieContainerBean() throws FileNotFoundException, IOException {
		Map<String, Map<String, KieContainer>> allContainers = new HashMap<>();
		Map<String, KieContainer> testContainers = new HashMap<>();
		Map<String, KieContainer> prodContainer = new HashMap<>();
		for (Resource file : HelperFunctions.getRuleFiles(Constants.RULES_PATH)) {
			String[] filePath = file.getURI().toString().split("/");
			KieContainer kieContainer = HelperFunctions
					.buildKieContainerAgain(Constants.RULES_PATH + filePath[filePath.length - 3] + "/");
			testContainers.put(filePath[filePath.length - 3], kieContainer);
			prodContainer.put(filePath[filePath.length - 3], kieContainer);
		}
		allContainers.put(Constants.TEST_ENVIRONMENT, testContainers);
		allContainers.put(Constants.PRODUCTION_ENVIRONMENT, prodContainer);
		KieContainerBean containerBean = new KieContainerBean(allContainers);
		return containerBean;
	}

	@Bean
	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		liquibase.setChangeLog(this.properties.getChangeLog());
		liquibase.setContexts(this.properties.getContexts());
		liquibase.setDefaultSchema(this.properties.getDefaultSchema());
		liquibase.setDropFirst(this.properties.isDropFirst());
		liquibase.setShouldRun(this.properties.isEnabled());
		liquibase.setLabels(this.properties.getLabels());
		liquibase.setChangeLogParameters(this.properties.getParameters());
		liquibase.setRollbackFile(this.properties.getRollbackFile());
		liquibase.setDatabaseChangeLogLockTable("CE_DATABASECHANGELOGLOCK");
		liquibase.setDatabaseChangeLogTable("CE_DATABASECHANGELOG");
		return liquibase;

	}
}
