package com.kuliza.lending.configurator.service.expressionservice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewExpression;
import com.kuliza.lending.configurator.pojo.SubmitNewExpressions;
import com.kuliza.lending.configurator.pojo.UpdateExpression;
import com.kuliza.lending.configurator.service.genericservice.GenericServicesCE;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;
import com.kuliza.lending.configurator.validators.ExpressionDataValidator;

@Service
public class ExpressionServices extends GenericServicesCE {

	private static final Logger logger = LoggerFactory.getLogger(ExpressionServices.class);
	@Autowired
	private GroupDao groupDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private ExpressionDao expressionDao;

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private ExpressionDataValidator expressionDataValidator;

	public GenericAPIResponse validateSubmitExpressions(SubmitNewExpressions input, BindingResult result, String userId,
			String productId, String groupId) throws Exception {
		logger.info("--> Entering validateSubmitExpressions()");
		Map<String, Object> data = new HashMap<>();
		GenericAPIResponse response = null;
		input.setUserId(userId);
		input.setProductId(productId);
		input.setGroupId(groupId);
		expressionDataValidator.validateNewExpression(input, result);
		response = checkErrors(result);
		Boolean isError = false;
		if (response == null) {
			List<NewExpression> expressions = input.getNewExpressionsList();
			List<Map<String, Object>> expressionErrors = new ArrayList<>();
			for (NewExpression expression : expressions) {
				BindingResult result2 = new BeanPropertyBindingResult(expression, "expression");
				// validates actual expression string.
				expression.setProductId(productId);
				expression.setGroupId(groupId);
				expressionDataValidator.validateExpression(expression, result2);
				if (result2.hasErrors()) {
					Map<String, Object> expressionError = new HashMap<>();
					expressionError.put(Constants.DATA_ERRORS_KEY,
							HelperFunctions.generateErrorResponseData(result2.getFieldErrors()));
					expressionErrors.add(expressionError);
					isError = true;
				} else {
					expressionErrors.add(null);
				}
			}
			if (isError) {
				logger.error("Error in Expressions::" + expressionErrors);
				data.put(Constants.DATA_ERRORS_KEY, expressionErrors);
				response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
						Constants.FAILURE_MESSAGE, data);
			}
		}
		logger.info("<-- Exiting validateSubmitExpressions()");
		return response;
	}

	public GenericAPIResponse validateUpdateExpression(String userId, String productId, String groupId,
			String expressionId, UpdateExpression input, BindingResult result) throws Exception {
		logger.info("--> Entering validateUpdateExpression()");
		GenericAPIResponse response = null;
		input.setUserId(userId);
		input.setProductId(productId);
		input.setGroupId(groupId);
		input.setExpressionId(expressionId);
		expressionDataValidator.validateUpdateExpression(input, result);
		response = checkErrors(result);
		if (response == null) {
			NewExpression newExpression = input.getNewExpression();
			BindingResult result2 = new BeanPropertyBindingResult(newExpression, "newExpression");
			// validates actual expression string.
			newExpression.setProductId(productId);
			newExpression.setGroupId(groupId);
			expressionDataValidator.validateExpression(newExpression, result2);
			response = checkErrors(result2);
		}
		logger.info("<-- Exiting validateUpdateExpression()");
		return response;
	}

	public GenericAPIResponse getExpressions(String productId, String groupId) throws Exception {
		logger.info("-->Entering getExpressions()");
		List<Expression> expressions = null;
		if (!groupId.equals("")) {
			logger.debug("getting expression for Group Id is:" + groupId);
			expressions = expressionDao.findByGroupIdAndIsDeleted(Long.parseLong(groupId), false);
		} else {
			logger.debug("getting expression for product Id is:" + productId);
			expressions = expressionDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false);
		}
		logger.info("<--Exiting getExpressions()");
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, expressions);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse createNewExpressions(SubmitNewExpressions input) throws Exception {
		logger.info("-->Entering createNewExpressions()");
		List<Expression> savedExpressions = new ArrayList<>();
		List<NewExpression> expressions = input.getNewExpressionsList();
		for (NewExpression expression : expressions) {
			Expression newExpression;
			if (!input.getGroupId().equals("")) {
				logger.debug("Creating new group Expression for groupId:" + input.getGroupId());
				Group group = groupDao.findByIdAndIsDeleted(Long.parseLong(input.getGroupId()), false);
				Product product = group.getProduct();
				String expressionString = HelperFunctions.makeStorableExpressionString(expression.getExpressionString(),
						variableDao, groupDao, Long.toString(product.getId()), true);
				newExpression = new Expression(expressionString, true, false, null, group);
			} else {
				logger.debug("Creating new product Expression for productId:" + input.getProductId());
				String expressionString = HelperFunctions.makeStorableExpressionString(expression.getExpressionString(),
						variableDao, groupDao, input.getProductId(), false);
				Product product = productDao.findByIdAndIsDeleted(Long.parseLong(input.getProductId()), false);
				newExpression = new Expression(expressionString, false, true, product, null);
			}
			expressionDao.save(newExpression);
			logger.debug("Successfully saved Expression");
			savedExpressions.add(newExpression);
		}
		logger.info("<--Exiting createNewExpressions()");
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, savedExpressions);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse updateExpression(UpdateExpression input) throws Exception {
		logger.info("--> Entering updateExpression()");
		NewExpression newExpression = input.getNewExpression();
		Expression oldExpression = expressionDao.findByIdAndIsDeleted(Long.parseLong(input.getExpressionId()), false);
		if (!input.getGroupId().equals("")) {
			logger.debug("updating group Expression for groupId:" + input.getGroupId());
			String expressionString = HelperFunctions.makeStorableExpressionString(newExpression.getExpressionString(),
					variableDao, groupDao, input.getProductId(), true);
			oldExpression.setExpressionString(expressionString);
		} else {
			logger.debug("updating product Expression for productId:" + input.getProductId());
			String expressionString = HelperFunctions.makeStorableExpressionString(newExpression.getExpressionString(),
					variableDao, groupDao, input.getProductId(), false);
			oldExpression.setExpressionString(expressionString);
		}
		//oldExpression.setModified(new Timestamp(new Date().getTime()));
		expressionDao.save(oldExpression);
		logger.debug("Successfully updated Expression");
		logger.info("<--Exiting updateExpression()");
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, oldExpression);
	}

}
