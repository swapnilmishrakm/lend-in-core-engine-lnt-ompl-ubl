package com.kuliza.lending.configurator.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "ce_product_category")
@Where(clause = "is_deleted=0")
public class ProductCategory extends BaseModel {

	@Column(unique = true, nullable = false)
	private String name;

	public ProductCategory() {
		super();
		this.setIsDeleted(false);
	}

	public ProductCategory(long id, String name) {
		super();
		this.setId(id);
		this.name = name;
		this.setIsDeleted(false);
	}

	public ProductCategory(String name) {
		this.name = name;
		this.setIsDeleted(false);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}