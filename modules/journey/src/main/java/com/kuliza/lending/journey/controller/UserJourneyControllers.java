package com.kuliza.lending.journey.controller;

import java.security.Principal;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.CustomerLoginInput;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.journey.service.UserJourneyServices;

@RestController
@RequestMapping("/lending/journey/")
public class UserJourneyControllers {

	@Autowired
	private UserJourneyServices userJourneyServices;

	@RequestMapping(method = RequestMethod.GET, value = "/initiate")
	public ResponseEntity<Object> initiateProcess(Principal principal,
			@RequestParam(required = true, value = "journeyName") String requestJourneyName) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.startOrResumeProcess(principal.getName(), requestJourneyName));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/submit-form")
	public ResponseEntity<Object> allSubmit(Principal principal, @RequestBody SubmitFormClass input) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.submitFormData(principal.getName(), input));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/submit-form")
	public ResponseEntity<Object> getPresentState(Principal principal, @RequestParam String processInstanceId) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.getFormData(principal.getName(), processInstanceId));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/back")
	public ResponseEntity<Object> moveBack(Principal principal, @Valid @RequestBody BackTaskInput input,
			BindingResult result) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.changeProcessState(principal.getName(), input, result));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public ResponseEntity<Object> loginUser(@RequestBody @Valid CustomerLoginInput customerLoginInput) {
		return CommonHelperFunctions.buildResponseEntity(userJourneyServices.loginUser(customerLoginInput));
	}

}
