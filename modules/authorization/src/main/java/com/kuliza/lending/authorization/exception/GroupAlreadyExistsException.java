package com.kuliza.lending.authorization.exception;

public class GroupAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = 6546272773782833251L;

	public GroupAlreadyExistsException() {
		super();
	}

	public GroupAlreadyExistsException(String message) {
		super(message);
	}

}
