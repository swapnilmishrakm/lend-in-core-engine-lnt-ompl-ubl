package com.kuliza.lending.authorization.config.keycloak;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.keycloak.admin.client.token.TokenService;

@Produces({ "application/json" })
@Consumes({ "application/x-www-form-urlencoded" })
public interface OpenIdService extends TokenService {

	@POST
	@Path("/realms/{realm}/protocol/openid-connect/logout")
	Response clearSession(@PathParam("realm") String var1, MultivaluedMap<String, String> var2);

	@POST
	@Path("/realms/{realm}/protocol/openid-connect/token/introspect")
	Response checkSession(@PathParam("realm") String var1, MultivaluedMap<String, String> var2);

}
