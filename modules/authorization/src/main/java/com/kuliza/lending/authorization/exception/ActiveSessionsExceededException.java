package com.kuliza.lending.authorization.exception;

public class ActiveSessionsExceededException extends RuntimeException {

	private static final long serialVersionUID = 1214382482837827929L;

	public ActiveSessionsExceededException(String message) {
		super(message);
	}

}
