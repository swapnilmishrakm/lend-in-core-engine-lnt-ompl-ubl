package com.kuliza.lending.engine_common.services.runtime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.exception.ProcessInstanceCompletedException;
import com.kuliza.lending.common.exception.ProcessInstanceDoesNotExistException;
import com.kuliza.lending.common.exception.ProcessInstanceNotCompletedException;
import com.kuliza.lending.common.utils.Constants;

@Service
public class RuntimeVariablesServices {

	@Autowired
	private RuntimeService runtimeService;

	private static final Logger logger = LoggerFactory.getLogger(RuntimeVariablesServices.class);

	/**
	 * 
	 * This functions provides all the process variables from parent as well as
	 * its child call activiti instances.
	 * 
	 * @param parentProcessInstanceId
	 * @return
	 * @throws NullPointerException
	 * @throws ProcessInstanceCompletedException
	 * @throws ProcessInstanceDoesNotExistException
	 * 
	 * @author Arpit Agrawal
	 */

	public Map<String, Object> getAllProcessVariablesFromParentProcessInstance(String parentProcessInstanceId)
			throws NullPointerException, ProcessInstanceCompletedException, ProcessInstanceDoesNotExistException {
		logger.info("--> Entering getAllProcessVariablesFromParentProcessInstance()");
		Map<String, Object> processVariables = new HashMap<>();
		if (parentProcessInstanceId == null) {
			throw new NullPointerException("parentProcessInstanceId is null");
		}
		if (!parentProcessInstanceId.matches(Constants.ENGINE_TABLES_ID_REGEX)) {
			throw new IllegalArgumentException(
					"parentProcessInstanceId should match regex : " + Constants.ENGINE_TABLES_ID_REGEX);
		} else {
			ProcessInstance parentProcessInstance = runtimeService.createProcessInstanceQuery()
					.processInstanceId(parentProcessInstanceId).includeProcessVariables().singleResult();
			if (parentProcessInstance != null) {
				if (parentProcessInstance.isEnded()) {
					throw new ProcessInstanceCompletedException(parentProcessInstanceId);
				} else {
					if (parentProcessInstance.getProcessVariables().isEmpty()) {
						logger.debug("Parent process variables empty. Only checking for child executions.");
					} else {
						processVariables = parentProcessInstance.getProcessVariables();
					}
					List<Execution> childProcessExecutions = runtimeService.createExecutionQuery()
							.rootProcessInstanceId(parentProcessInstance.getId()).list();
					for (Execution singleExecution : childProcessExecutions) {
						logger.debug("Checking child process instance : " + singleExecution.getProcessInstanceId()
								+ " variable for parent process instance : " + parentProcessInstanceId);
						ProcessInstance childProcessInstance = runtimeService.createProcessInstanceQuery()
								.processInstanceId(singleExecution.getProcessInstanceId()).includeProcessVariables()
								.singleResult();
						processVariables.putAll(childProcessInstance.getProcessVariables());
					}
					processVariables.put("PROC_INST_ID", parentProcessInstance.getId());
					logger.info("<-- Exiting getAllProcessVariablesFromParentProcessInstance()");
					return processVariables;
				}
			} else {
				throw new ProcessInstanceDoesNotExistException(parentProcessInstanceId);
			}

		}
	}

}
