package com.kuliza.lending.backoffice.dao;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Buckets;
import com.kuliza.lending.backoffice.models.BucketsVariablesMapping;
import com.kuliza.lending.backoffice.models.Variables;

@Repository
public interface BucketsVariablesDao extends CrudRepository<BucketsVariablesMapping, Long> {

	public BucketsVariablesMapping findByBucketAndVariableAndIsDeleted(Buckets bucket, Variables variable,
			boolean isDeleted);

	public Set<BucketsVariablesMapping> findByBucketAndIsDeletedOrderByBucketVariableOrderAsc(Buckets bucket,
			boolean IsDeleted);

}
