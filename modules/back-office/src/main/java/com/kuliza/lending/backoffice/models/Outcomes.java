package com.kuliza.lending.backoffice.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_outcomes", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "outcomeKey", "role_id" }) })
public class Outcomes extends BaseModel {

	@Column(nullable = false)
	private String outcomeKey;

	@Column(nullable = false)
	private int outcomesOrder;

	@Column(nullable = false)
	private String label;

	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean commentRequired = true;

	@ManyToOne()
	@JoinColumn(name = "role_id")
	private Roles role;

	public Outcomes() {
		super();
		this.setIsDeleted(false);
	}

	public Outcomes(String outcomeKey, int outcomesOrder, String label, boolean commentRequired, Roles role) {
		super();
		this.outcomeKey = outcomeKey;
		this.outcomesOrder = outcomesOrder;
		this.label = label;
		this.commentRequired = commentRequired;
		this.role = role;
		this.setIsDeleted(false);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getOutcomeKey() {
		return outcomeKey;
	}

	public void setOutcomeKey(String outcomeKey) {
		this.outcomeKey = outcomeKey;
	}

	public int getOutcomesOrder() {
		return outcomesOrder;
	}

	public void setOutcomesOrder(int outcomesOrder) {
		this.outcomesOrder = outcomesOrder;
	}

	public boolean isCommentRequired() {
		return commentRequired;
	}

	public void setCommentRequired(boolean commentRequired) {
		this.commentRequired = commentRequired;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

}
