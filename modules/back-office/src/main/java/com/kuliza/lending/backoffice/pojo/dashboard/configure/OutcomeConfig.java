package com.kuliza.lending.backoffice.pojo.dashboard.configure;

public class OutcomeConfig {

	private String label;
	private String key;
	private boolean commentRequired;

	public OutcomeConfig() {
		super();
	}

	public OutcomeConfig(String label, String key, boolean commentRequired) {
		super();
		this.label = label;
		this.key = key;
		this.commentRequired = commentRequired;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isCommentRequired() {
		return commentRequired;
	}

	public void setCommentRequired(boolean commentRequired) {
		this.commentRequired = commentRequired;
	}

}
