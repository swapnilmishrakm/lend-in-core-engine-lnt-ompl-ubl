package com.kuliza.lending.backoffice.pojo.dashboard.config;

public class OptionsConfig {

	String label;
	String key;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public OptionsConfig(String label, String key) {
		super();
		this.label = label;
		this.key = key;
	}

	public OptionsConfig() {
		super();
	}

}
