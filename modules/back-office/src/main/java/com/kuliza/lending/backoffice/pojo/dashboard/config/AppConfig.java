package com.kuliza.lending.backoffice.pojo.dashboard.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This Class is used for getting buckets and their variables configuration as
 * List
 * 
 * @author kuliza-330
 */
public class AppConfig {

	private List<BucketVariablesConfig> buckets;

	public AppConfig() {
		super();
	}

	public AppConfig(List<BucketVariablesConfig> buckets) {
		super();
		this.buckets = buckets;
	}

	public AppConfig(Set<BucketVariablesConfig> buckets) {
		super();
		this.buckets = new ArrayList<>();
		for (BucketVariablesConfig bucket : buckets) {
			this.buckets.add(new BucketVariablesConfig(bucket));
		}

	}

	public List<BucketVariablesConfig> getBuckets() {
		return buckets;
	}

	public void setBuckets(List<BucketVariablesConfig> buckets) {
		this.buckets = buckets;
	}

}
