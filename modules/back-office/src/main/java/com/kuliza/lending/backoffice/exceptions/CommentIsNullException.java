package com.kuliza.lending.backoffice.exceptions;

public class CommentIsNullException extends RuntimeException {

	public CommentIsNullException() {
		super();
	}

	public CommentIsNullException(String message) {
		super(message);
	}

}
